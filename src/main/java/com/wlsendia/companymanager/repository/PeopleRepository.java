package com.wlsendia.companymanager.repository;

import com.wlsendia.companymanager.entity.People;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PeopleRepository extends JpaRepository<People, Long> {
}
