package com.wlsendia.companymanager.controller;

import com.wlsendia.companymanager.model.PeopleItem;
import com.wlsendia.companymanager.model.PeopleRequest;
import com.wlsendia.companymanager.repository.PeopleRepository;
import com.wlsendia.companymanager.service.PeopleService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/people")
public class PeopleController {
    private final PeopleService peopleService;
    private final PeopleRepository peopleRepository;

    @PostMapping("/data")
    // vaild (시스템이 쓰기)유효한, validation 확인
    public String setPeople(@RequestBody @Valid PeopleRequest peopleRequest)/*입력부*/ {
        peopleService.setPeople(
                peopleRequest.getName(),
                peopleRequest.getPhone(),
                peopleRequest.getBirth(),
                peopleRequest.getGender(),
                peopleRequest.getAge(),
                peopleRequest.getEmail()
        );

        return "0";
    }

    @GetMapping("/peoples")
    public List<PeopleItem> getPeoples()/*출력부*/ {
        List<PeopleItem> result = peopleService.getPeoples();
        return result;
    }

}
// 내일 예