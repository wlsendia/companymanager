package com.wlsendia.companymanager.service;

import com.wlsendia.companymanager.entity.People;
import com.wlsendia.companymanager.model.PeopleItem;
import com.wlsendia.companymanager.repository.PeopleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PeopleService {

    private final PeopleRepository peopleRepository;

    public void setPeople(String name, String phone, LocalDate birth, Boolean gender, Integer age, String email) {
        People addData = new People();
        addData.setName(name);
        addData.setPhone(phone);
        addData.setBirth(birth);
        addData.setGender(gender);
        addData.setAge(age);
        addData.setEmail(email);

        peopleRepository.save(addData);

    }

    public List<PeopleItem> getPeoples() {
        List<PeopleItem> result = new LinkedList<>();

        List<People> originData = peopleRepository.findAll();

        for (People item : originData) {
            PeopleItem addItem = new PeopleItem();
            addItem.setName(item.getName());
            addItem.setPhone(item.getPhone());

            result.add(addItem);
        }

        return result;
    }

}
