package com.wlsendia.companymanager.model;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.Value;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;
import java.time.LocalDate;

@Getter
@Setter
public class PeopleRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String name;

    @NotNull
    @Length(min = 11, max = 20)
    private String phone;

    @NotNull
    private LocalDate birth;

    //@Min(0)
    //@Max(1)
    private Boolean gender;//선택, 0이 남자

    //@Min(0)
    //@Max(150)
    private Integer age;//선택


    @Email
    private String email;//선택
}
